/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.hashmap;

/**
 *
 * @author WIP
 * @param <Key>
 * @param <Value>
 */
public class Hash<Key, Value> {

    private final int M = 8;
    private final Value[] vals = (Value[]) new Object[M];
    private final Key[] keys = (Key[]) new Object[M];
    private int map;

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % M;
    }

    public void put(Key key, Value val) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = val;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }

    public int remove(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                int temp = (int) keys[i];
                keys[i] = null;
                return temp;
            }
            i = (i + 1) % map;
        }
        return 0;
    }

}
